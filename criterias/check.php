<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');

include_once '../config/database.php';
include_once '../objects/criterias.php';
include_once '../objects/rekomendasi.php';
include_once '../objects/plants.php';

$database = new Database();
$db = $database->getConnection();

$criterias = new Criterias($db);
$rekomendasi = new Rekomendasi($db);
$plants = new Plants($db);
$criterias->token = isset($_GET['token']) ? $_GET['token'] : die();
$wp_res = array();
$stmt1 = $criterias->readCriteria();

$W = array();
$attribute = array();


while ($row = $stmt1->fetchObject()) {
    $W[$row->id_criteria] = $row->weight;
    $attribute[$row->id_criteria] = $row->attribute;

}

$reverse = array_reverse($W,true);
$W = $reverse;
$bobotawal = array(
  "bobot_awal" =>  $reverse
);
$wp_res["bobot_awal"] = $reverse;
// array_push($wp_res["normalisasi"], $bobotawal);
//normalisasi bobot

$wp_res["normalisasi"] = array();
$i = 0;
$sigma_w = array_sum($W);
$w2 = $W;
foreach ($W as $j => $w) {
  $count_normal = "W".(++$i)."</sub>= {$w} /(".implode('+',$w2).") = {$w} / {$sigma_w} = ".($w/$sigma_w)."<br>";
  $normal = array(

    "hitung" => $count_normal
);
    $W[$j] = $w / $sigma_w;
    array_push($wp_res["normalisasi"], $normal);
}


//hitung vektor S
$stmt2 = $rekomendasi->read();
$wp_res["vektor_s"] = array();
$X = array();
$alternative = '';
while ($row = $stmt2->fetchObject()) {
    if ($row->id_plant != $alternative) {
        $X[$row->id_plant] = array();
        $alternative = $row->id_plant;
    }
    $X[$row->id_plant][$row->id_criteria] = $row->weight;
}
$S = array();
$c = 0;
foreach ($X as $alternative => $x) {
    $S[$alternative] = 1;
      $VSKode = array(
            "hitung" => "S".(++$c)."="
      );
      array_push($wp_res["vektor_s"], $VSKode);
    foreach ($x as $criteria => $weight) {
        $S[$alternative] *= pow($weight, ($attribute[$criteria] == 'cost' ? -$W[$criteria] : $W[$criteria]));
        $count_vektor_s = "({$weight}<sup>".($attribute[$criteria]=='cost'?-$W[$criteria]:$W[$criteria])."</sup>)";
        $vektorsarr = array(

          "hitung" => $count_vektor_s
      );
      array_push($wp_res["vektor_s"], $vektorsarr);
     
    };
    $result_vektor_s = " = {$S[$alternative]}<br>";
    $vektorsresult = array(

      "hitung" => $result_vektor_s
  );
    array_push($wp_res["vektor_s"], $vektorsresult);

}

//hitung vektor V
$wp_res["vektor_v"] = array();

$rank = array();
$V = array();
$sigma_s = array_sum($S);
$counter = 0;
$divider = implode(' + ', $S);
foreach ($S as $alternative => $s) {
  $vektorv_value = "V".(++$counter)."= $s/({$divider})= ";
  $result_vektorv = array(
    "hitung" => $vektorv_value
  );
  array_push($wp_res["vektor_v"], $result_vektorv);
    $V[$alternative] = $s / $sigma_s;
  $resultV = array(
    "hitung" => "<span>".$V[$alternative]."</span><br>"
  );
  array_push($wp_res["vektor_v"], $resultV);
  $count = "counter";
  $key = "V".$counter;
  $rank[$key]= $V[$alternative];
 
  
}





//rank vektor V
arsort($V);
arsort($rank);
$no = 0;
$wp_res["rank_v"] = array();
$obj = new stdClass();
foreach($rank as $key => $value)
{
  $obj->$key = $value;
  $sorted = array(
    "nama" => $key,
    "value" => $value
  );

  array_push($wp_res["rank_v"], $sorted);
}

$wp_res["records"] = array();



foreach ($V as $alternative => $result) {
    $stmt3 = $plants->readName($alternative);

    $alt_name = "";
    $alt_desc = "";

    while ($row = $stmt3->fetchObject()) {
        $alt_name = $row->plant_name;
        $alt_desc = $row->plant_desc;
    }

  
    
    // array_push($wp_res["rank_v"], $rank);
    $result_rank = array(

        "nama" => $alt_name,
        "desc" => $alt_desc,
        "result" => $result,
    );
    array_push($wp_res["records"], $result_rank);

}
echo json_encode($wp_res);
